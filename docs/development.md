# Development

All the development is done on [Gitlab](https://gitlab.com/galacteek),
a phenomenal coding platform with amazing CI features.

## Repositories

### galacteek

This is the main repository. It contains the code for the browser
and the various services, including *pronto*.

- [https://gitlab.com/galacteek/galacteek](https://gitlab.com/galacteek/galacteek)

### galacteek-installer

Repository with the CI to build the installers for the various platforms.

- [https://gitlab.com/galacteek/galacteek-installer](https://gitlab.com/galacteek/galacteek-installer)

### galacteek-dvoz

[dvoz](../capsules/dvoz) is the main dapp and uses [pronto](docs/pronto.md) to create
objects in the distributed graphs.

- [https://gitlab.com/galacteek/galacteek-dvoz](https://gitlab.com/galacteek/galacteek-dvoz)

### icapsule-qml-gforce

*gforce* is the main *icapsule*, and has all the QML components needed
to write a dapp that interacts with IPFS and the RDF graphs.

- [https://gitlab.com/galacteek/icapsule-qml-gforce](https://gitlab.com/galacteek/icapsule-qml-gforce)

### galacteek-ld-web4

This repository contains the JSON-LD schemas for the **ips://galacteek.ld/**
schemas database.

- [https://gitlab.com/galacteek/galacteek-ld-web4](https://gitlab.com/galacteek/galacteek-ld-web4)

### hashmarks-dwebland

This repository contains the main hashmarks dataset defined as
linked data.

- [https://gitlab.com/galacteek/hashmarks-dwebland](https://gitlab.com/galacteek/hashmarks-dwebland)

### dataset-itags-core

This repository contains the tags dataset defined as
linked data.

- [https://gitlab.com/galacteek/dataset-itags-core](https://gitlab.com/galacteek/dataset-itags-core)

### rdflib and rdflib-json-ld

This repository contains a customized version of the *rdflib* python
library (for RDF support), tuned to be able to load JSON-LD schemas
from IPFS. *rdflib-json-ld* is also modified to be able to transform
individual RDF resources to JSON-LD.

- [https://gitlab.com/galacteek/rdflib](https://gitlab.com/galacteek/rdflib)
- [https://gitlab.com/galacteek/rdflib-json-ld](https://gitlab.com/galacteek/rdflib-json-ld)

### aiogeminipfs

Fork of [aiogemini](https://github.com/keis/aiogemini). Can render
UnixFS objects with the gemini protocol.

- [https://gitlab.com/galacteek/aiogeminipfs](https://gitlab.com/galacteek/aiogeminipfs)
