---
title: P2P browser
---

[![logo](https://gitlab.com/galacteek/galacteek/-/raw/pimp-my-dweb/share/icons/galacteek.png){: style="width:256px; height:256px" }](https://gitlab.com/galacteek/galacteek)

**This software is no longer actively maintained and will not receive any major updates. Performance issues, and the deprecation/removal of the pubsub subsystem in kubo, which galacteek heavily relies upon, make it difficult to maintain.**{class="unmaintained"}

[galacteek](https://gitlab.com/galacteek/galacteek) is a multi-platform
P2P browser and a *dapps* (aka *distributed apps*) platform for the
distributed web.
It relies on the [IPFS](https://ipfs.io) peer-to-peer technology.

*galacteek* peers exchange *linked data* over peer-to-peer, encrypted
channels. The goal of this P2P linked data engine is to facilitate
serverless, censorship-free person-to-person communications,
using an [evolvable vocabulary](https://gitlab.com/galacteek/galacteek-ld-web4/-/tree/master/galacteek_ld_web4/contexts/galacteek.ld). Take a [look at dvoz](capsules/dvoz), the
core dapp.

The P2P *dapps* are written with the
[QML language](https://en.wikipedia.org/wiki/QML) and are stored on
the distributed web (with *IPFS*). For more information on how to
use dapps, see the [dapps documentation](docs/dapps)

**Installation with Flatpak**:

```sh
--8<-- "flatpak-bundle-install.sh"
```

[![Download](download.png){: style="width:128px; height:128px" }](download.md)  [![Flatpak](img/flatpak-logo.jpg){: style="width:128px; height:auto" }](download.md#flatpak) [![dvoz](https://gitlab.com/galacteek/galacteek-dvoz/-/raw/purple/share/icons/dvoz.png){: style="width:128px; height:auto" }](capsules/dvoz) [![Docker image](docker.jpg){: style="width:128px; height:128px" }](running-a-node.md)  [![Demos](demo.png){: style="width:128px; height:128px" }](demo.md)  [![Telegram channel](telegram.png){: style="width:128px; height:128px" }](https://t.me/Galacteek)  [![Matrix room](matrix.svg){: style="width:128px; height:auto" }](https://matrix.to/#/#galacteek:matrix.org)  [![Gitlab](img/gitlab.png){: style="width:128px; height:128px" }](development.md)
--8<-- "sponsor-platforms.md"

![type:video](./videos/galacteek-demo-main.mp4)

[Go to this page to download the software](download.md){#download}

## Known issues

- [#15](https://gitlab.com/galacteek/galacteek/-/issues/15): when the app is
  started for the first time, the RDF datasets don't seem to be commited
  correctly, and some of the data in the graph won't appear in the UI.
  After **restarting the app**, the datasets will be merged again, this time
  correctly. No known fixes for this issue yet.

## Contact

You can send an email [here](mailto: galacteek@protonmail.com).

## Sponsor this project

See [the sponsor page](sponsor.md)
