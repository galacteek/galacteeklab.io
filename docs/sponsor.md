---
title: Sponsor this project
---

You can help this project by becoming a sponsor via the following
platforms ([Ko-fi](https://ko-fi.com/galacteek) or
[Liberapay](https://liberapay.com/galacteek/donate) are the preferred platforms). [Please send an email](mailto: galacteek@protonmail.com) if you make a crypto donation.

Thank you to all who've donated in the past or who are currently sponsoring.

--8<-- "sponsor-platforms.md"

# Bitcoin

BTC: **1Cgwbzi6R4TgWp7AG67BPwYY1iz15ATR7A**

![BTC donate](btc-donate.png)]

# Monero

XMR: **46CAwqF4zwjL3Pe1oxtr2AGbmzLFY4rp6eAGkiDbAfYnYCAx8C47dex2XGb6VqXDUoYEZmemLzKxT1wkjFU6ebk23DensvR**

# Ethereum

ETH: **0xaB3C8911D5C59bAdF797C546D7Fb56A00e4355dd**
