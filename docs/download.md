---
title: Download galacteek
---

If you have any trouble running the image, [please consult this section](#running).
Don't forget to [install gstreamer](#installing-gstreamer-linux) if you run it
on Linux.

# Download

[![Flatpak](img/flatpak-logo.jpg){: style="width:64px; height:auto" }](#flatpak-bundle) [Download the latest Flatpak bundle (Linux)](https://gateway.pinata.cloud/ipfs/bafybeibwteyznsdmctnpxsacwz4yaiy4mmpm3vpjveahjzbuve7mk6d75i/Galacteek-f9629997-x86_64.flatpak){#release} ([read this](#flatpak-bundle) if you don't know how to install Flatpak bundles)

[![AppImage](img/appimage.png){: style="width:64px; height:auto" }](https://gateway.pinata.cloud/ipfs/bafybeihkzbxjup4yxtxk6gcl3kjj2porgjrrkeqjg6oe72clhdlap6ae6u/Galacteek-f9629997-x86_64.AppImage) [Download the latest AppImage (Linux)](https://gateway.pinata.cloud/ipfs/bafybeihkzbxjup4yxtxk6gcl3kjj2porgjrrkeqjg6oe72clhdlap6ae6u/Galacteek-f9629997-x86_64.AppImage){#release}

[![DMG](img/macos-wordmark.png){: style="width:64px; height:auto" }](https://github.com/pinnaculum/galacteek-installer/releases/download/ae658dfc/Galacteek-ae658dfc-x86_64.dmg) [Download the latest DMG (MacOS)](https://github.com/pinnaculum/galacteek-installer/releases/download/ae658dfc/Galacteek-ae658dfc-x86_64.dmg){#release}

[![NSI](img/windows-logo.png){: style="width:64px; height:auto" }](https://github.com/pinnaculum/galacteek-installer/releases/download/ca9d79f0/Galacteek-ca9d79f0-installer-x86_64.exe) [Download the latest NSI (Windows)](https://github.com/pinnaculum/galacteek-installer/releases/download/ca9d79f0/Galacteek-ca9d79f0-installer-x86_64.exe){#release}

[CHANGELOG](https://gitlab.com/galacteek/galacteek/-/blob/pimp-my-dweb/CHANGELOG.md) | [Telegram channel](https://t.me/Galacteek) | [Matrix room](https://matrix.to/#/#galacteek:matrix.org).

## On startup

You should see a popup like this when you first run it
(or during upgrades):

![type:video](./videos/galacteek-starter.gif)

## Platform notes

- *Windows*: the NSI installers are currently weekly snapshots and do
not use live upgrades.

# Flatpak bundle

Install the [Flatpak](https://flatpak.org/) bundle as a non-root user
by running these commands in a terminal (you will need *sudo* rights) :

```sh
--8<-- "flatpak-bundle-install.sh"
```

Or install the bundle only for your user (no specific permissions required,
however it might not appear in the applications menu) :

```sh
--8<-- "flatpak-bundle-install-user.sh"
```

The application should appear as *Galacteek* in the *Internet* submenu
of your Linux desktop's applications menu. You can also run it from
a terminal:

```sh
--8<-- "flatpak-run.sh"
```

**Note**: Flatpak stores the application's data files in a separate
folder (by default *.var/app* in your *HOME*), different from what
the AppImage uses.

# Running

## Installing gstreamer (Linux)

To use the mediaplayer inside galacteek you will need to have the
*gstreamer* libraries (you'll need *gstreamer 1.0*) installed on your system.
To install gstreamer and the main plugins on a Debian/Ubuntu system, run the
following:

```sh
--8<-- "gstreamer-install-debian.sh"
```

If you use another type of Linux distribution, search for and install the
*gstreamer 1.0* packages and the *good*, *bad* and *ugly* plugins.

## How to run the AppImage (Linux)

After downloading the AppImage, make it executable and then run it from the
shell or double-click on it from the desktop. Example:

```sh
--8<-- "appimage-run.sh"
```

## How to run with Flatpak (Linux)

```sh
--8<-- "flatpak-run.sh"
```

## Command-line flags

This section describes the various flags that you can pass when you run
the AppImage.

### config-defaults

Use **--config-defaults** to restore the default configuration.

**Only use this flag if the UI won't start !**

### d

Use **-d** to log debugging messages.

## goipfs-debug

Use **--goipfs-debug** to log debugging messages from *go-ipfs*.

### profile

Use **--profile** with a *profile name* to use a custom application profile.
The default profile is called *main*. Example:

```sh
--8<-- "appimage-run-profile-demo.sh"
```

## How to run the DMG image (MacOS)

After opening/mounting the DMG image, **hold the Control key** and click on
the galacteek icon, select Open and then accept. You probably need to allow
the system to install applications from anywhere in the security settings.

# Release workflow

AppImages and Flatpak bundles are built from GitLab CI.
They are distributed via IPFS, which makes
it easier to replicate (a big thanks to [Pinata](https://pinata.cloud) for
their amazing service !).

At the moment, the DMG and NSI installers are built from GH Actions.
