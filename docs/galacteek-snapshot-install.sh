#!/bin/bash
#
# galacteek: automatic appimage download & execution script
#

set -e

APPIMAGE_GITLAB_URL="https://gitlab.com/galacteek/galacteek-installer/-/releases/continuous-pimp-my-dweb/downloads/Galacteek-pimp-my-dweb-latest-x86_64.AppImage"

abort() {
    printf "%s\n" "$@"
    exit 1
}

get_pmd_appimage_url() {
  curl --head --silent \
       --write-out "%{redirect_url}\n" --output /dev/null \
       ${APPIMAGE_GITLAB_URL}
}

os=$( echo "$(uname -s)" | tr -s '[:upper:]' '[:lower:]' )
arch=$( echo "$(uname -m)" | tr -s '[:upper:]' '[:lower:]' )

if [ "$arch" != "x86_64" ]; then
    abort "This is not an x64 system"
fi

dstdir=${HOME}/Downloads/Galacteek

mkdir -p $dstdir

url_latest=$(get_pmd_appimage_url)

if [ -z "${url_latest}" ]; then
    abort "Could not find the latest snapshot URL"
fi

fname=$(basename $url_latest)
appimagef="${dstdir}/${fname}"

APPIMAGE_SHA_URL="https://gitlab.com/galacteek/galacteek-installer/-/releases/continuous-pimp-my-dweb/downloads/${fname}.sha512"

APPIMAGE_SHA=$(curl --location --progress-bar ${APPIMAGE_SHA_URL})

if [ -z "${APPIMAGE_SHA}" ]; then
    abort "Could not retrieve the AppImage SHA512"
fi

if [ -f "${appimagef}" ]; then
    SHA_EX=$(sha512sum "$appimagef"|awk '{print $1}')

    if [ "${APPIMAGE_SHA}" != "${SHA_EX}" ]; then
        echo "=> SHA mismatch, removing invalid download"
        rm -f ${appimagef}
    fi
fi

if [ ! -f "${appimagef}" ]; then
    echo "=> Downloading AppImage from: $url_latest"
    curl --fail --progress-bar --output ${appimagef} ${url_latest}
    echo "=> Downloaded AppImage to: ${appimagef}"
fi

SHA_EX=$(sha512sum "$appimagef"|awk '{print $1}')

if [ "${APPIMAGE_SHA}" = "${SHA_EX}" ]; then
    echo "=> Running AppImage: ${appimagef}"

    chmod +x ${appimagef}
    ${appimagef}
fi
