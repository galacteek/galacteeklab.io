# Developpement

Le développement se passe sur [Gitlab](https://gitlab.com/galacteek).

## Repositories

### galacteek

Le repos git principal: contient le code de l'interface du navigateur et
des services principaux.

- [https://gitlab.com/galacteek/galacteek](https://gitlab.com/galacteek/galacteek)

### galacteek-installer

- [https://gitlab.com/galacteek/galacteek-installer](https://gitlab.com/galacteek/galacteek-installer)

### galacteek-dvoz

*dvoz* est la *dapp* principale. *dvoz* utilise [pronto](docs/pronto.md) pour
le modèle de données (graphe RDF distribué).

- [https://gitlab.com/galacteek/galacteek-dvoz](https://gitlab.com/galacteek/galacteek-dvoz)

### icapsule-qml-gforce

*gforce* est la principale *icapsule*, et contient tous les composants QML
requis pour écrire une dapp complète qui interagit avec IPFS et les graphes
RDF.

- [https://gitlab.com/galacteek/icapsule-qml-gforce](https://gitlab.com/galacteek/icapsule-qml-gforce)

### galacteek-ld-web4

Ce repos contient les schemas JSON-LD pour la base de données de schemas
**ips://galacteek.ld/**.

- [https://gitlab.com/galacteek/galacteek-ld-web4](https://gitlab.com/galacteek/galacteek-ld-web4)

### hashmarks-dwebland

Ce repos contient le dataset de hashmarks (bookmarks pour le dweb).

- [https://gitlab.com/galacteek/hashmarks-dwebland](https://gitlab.com/galacteek/hashmarks-dwebland)

### dataset-itags-core

Ce repos contient le principal dataset de itags.

- [https://gitlab.com/galacteek/dataset-itags-core](https://gitlab.com/galacteek/dataset-itags-core)



### rdflib and rdflib-json-ld

Ce repos est une version altérée de la librairie python *rdflib* (pour
le support RDF). Ce *fork* permet de charger les schemas JSON-LD
depuis IPFS. *rdflib-json-ld* est aussi modifiée pour pouvoir transformer
des resources RDF distinctes en JSON-LD.

- [https://gitlab.com/galacteek/rdflib](https://gitlab.com/galacteek/rdflib)
- [https://gitlab.com/galacteek/rdflib-json-ld](https://gitlab.com/galacteek/rdflib-json-ld)

### aiogeminipfs

*Fork* de [aiogemini](https://github.com/keis/aiogemini). Permet
de servir des objets IPFS UnixFS (fichiers) via le protocole gemini.

- [https://gitlab.com/galacteek/aiogeminipfs](https://gitlab.com/galacteek/aiogeminipfs)
