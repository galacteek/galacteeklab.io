# Model

One of the main problems that the "modern web" suffers from is that there is no
well-defined data model (it is *document-based*, you could argue that the *document*,
the DOM, is the model). This, combined with the fact that it's
completely locked in the *server-client* paradigm, means you're never in
control of what happens with your data.

*dvoz*, like many Qt-based apps, relies heavily on the MVC
(model-view-controller) design pattern. In this case, the model
is a *SparQL model* (a *Qt* model that contains the results
of a SparQL query executed on one of the RDF graphs). The view can be anything
you want (a *ListView* is the most common).

With *galacteek*, the peers exchange linked data (through *SparQL* queries). If
a person writes an article, or a forum post, other peers will pull these
objects and merge it in their RDF graph. *dvoz* offers a simple presentation
(*view*) of the data in the graph. Because everything is *linked*, you can
*reply to*, or *follow* anything that others have published.

# Contribute

If you have skills in QML, your help is more than welcome ! Improving the
forum sections dataset is also a priority.

- [dvoz GitLab repository](https://gitlab.com/galacteek/galacteek-dvoz/-/tree/purple).
