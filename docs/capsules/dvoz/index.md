---
title: Dvoz
---

![logo](https://gitlab.com/galacteek/galacteek-dvoz/-/raw/purple/share/icons/dvoz.png){: style="width:64px; height:auto" }

**dvoz** (short for *decentralized voice*, *voz* meaning *voice* in Spanish)
is an offline-first decentralized social exchange application
built with the QML language.

![gif](/img/dvoz-home.gif){: style="width: 75%; height:auto" }

## Browsing gemini capsules

![type:video](https://bafybeib2o6ekyemwqu2t6dcj3mkfjm5kxl2ial5wspg3yyfphgjltooame.ipfs.dweb.link/dvoz-gemini-browsing.mp4)

## Articles

![type:video](https://bafybeidlwtq6my6mctljowd73qwov6nlfqh233jbl7nhratf6haettpwku.ipfs.dweb.link/dvoz-articles-list-react.mp4)

## Gempub

![type:video](https://bafybeig4snejglr23pbit6wkbkspxvo2inoxprfl4x5wbcmy7wjikjnsmu.ipfs.dweb.link/dvoz-gempub.mp4)

![type:video](https://bafybeifibdh524pnn5h5smy5w6f7bsp2thjmpbtl3d3qkzwdtvcuvhkvii.ipfs.dweb.link/dvoz-gempub-lib.mp4)

- [dvoz model](./dev.md#model)
- [dvoz GitLab repository](https://gitlab.com/galacteek/galacteek-dvoz/-/tree/purple).
- [dvoz schemas (galacteek-ld-web4)](https://gitlab.com/galacteek/galacteek-ld-web4)
