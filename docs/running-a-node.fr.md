---
title: Lancer une node avec docker
---

# Comment faire tourner une node

Pour lancer une instance de *galacteek* comme un service, vous n'avez
besoin que d'un système Linux équipé de Docker.

En faisant cela vous aidez a tester le protocole **pronto**. Pour savoir
comment marche **pronto**, [consultez cette page](docs/pronto.md).

Pour télécharger l'image Docker et faire tourner une instance, utilisez
les commandes suivantes (image de la branche *pimp-my-dweb*):

```
mkdir $HOME/galacteek-docker
sudo docker pull registry.gitlab.com/galacteek/galacteek:pimp-my-dweb-latest-x11vnc
sudo docker run -p 5900:5900 -v $HOME/galacteek-docker:/home/galacteek -t registry.gitlab.com/galacteek/galacteek:pimp-my-dweb-latest-x11vnc
```

Dans cet exemple, les données et graphes utilisés par *galacteek* seront
persistés dans le répertoire *$HOME/galacteek-docker*.

Le mot de passe pour accéder au service *VNC* est affiché sur la console
lors du démarrage. Vous pouvez trouver l'adresse IP du container Docker en
lancant:

```
docker inspect <container-id>|grep IPAddress
```

Vous pouvez maintenant utiliser n'importe quel client *VNC* pour accéder
a l'interface de l'instance (port TCP: **5900**).
