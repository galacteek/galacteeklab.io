[![logo](https://gitlab.com/galacteek/galacteek/-/raw/pimp-my-dweb/share/icons/galacteek.png){: style="width:256px; height:256px" }](https://gitlab.com/galacteek/galacteek)

[galacteek](https://gitlab.com/galacteek/galacteek) es un navegador
que permite el acceso a la web distribuida (la llamada *dweb*) y una plataforma
de aplicaciones P2P distribuidas (*dapps*). Usa el protocolo
P2P (*Par-a-Par*) [IPFS](https://ipfs.io).

Las *dapps* P2P estan desarolladas con el
[lenguaje QML](https://es.wikipedia.org/wiki/QML) y estan almacenadas
con IPFS. Para tener mas informacion sobre la utilización de las *dapps* en
*galacteek*, [consulten la documentacion](docs/dapps/tutorial.md)

**Instalación con Flatpak**:

```sh
--8<-- "flatpak-bundle-install.sh"
```

[![Descargar](download.png){: style="width:128px; height:128px" }](download.md) [![Flatpak](img/flatpak-logo.jpg){: style="width:128px; height:auto" }](download.md#flatpak) [![Imagenes Docker](docker.jpg){: style="width:128px; height:128px" }](running-a-node.md) [![Demos](demo.png){: style="width:128px; height:128px" }](demo.md) [![Telegram channel](telegram.png){: style="width:128px; height:128px" }](https://t.me/Galacteek) [![Matrix room](matrix.svg){: style="width:128px; height:auto" }](https://matrix.to/#/#galacteek:matrix.org) [![Gitlab](img/gitlab.png){: style="width:128px; height:128px" }](development.md)
--8<-- "sponsor-platforms.md"

![type:video](./videos/galacteek-demo-main.mp4)

[Haz clic aqui para descargar galacteek](download.md){#download}

# Ejecutar galacteek con Docker

Se puede ejecutar *galacteek* en un contenedor Docker, para mas informacion
[consulten esta pagina](running-a-node.md)

## Esponsorizar galacteek

[Página de donaciones](sponsor.md)
