---
title: Télécharger galacteek
---

Si vous avez un problème pour lancer l'image,
[regardez cette section](#lancement).

# Télécharger

[![Flatpak](img/flatpak-logo.jpg){: style="width:64px; height:auto" }](#flatpak) [Télécharger le paquet Flatpak (Linux)](https://gateway.pinata.cloud/ipfs/bafybeibwteyznsdmctnpxsacwz4yaiy4mmpm3vpjveahjzbuve7mk6d75i/Galacteek-f9629997-x86_64.flatpak){#release} ([lisez ceci](#flatpak-bundle) si vous ne savez pas comment installer des bundles Flatpak)

[![AppImage](img/appimage.png){: style="width:64px; height:auto" }](https://gateway.pinata.cloud/ipfs/bafybeihkzbxjup4yxtxk6gcl3kjj2porgjrrkeqjg6oe72clhdlap6ae6u/Galacteek-f9629997-x86_64.AppImage) [Télécharger l'AppImage (Linux)](https://gateway.pinata.cloud/ipfs/bafybeihkzbxjup4yxtxk6gcl3kjj2porgjrrkeqjg6oe72clhdlap6ae6u/Galacteek-f9629997-x86_64.AppImage){#release}

[![DMG](img/macos-wordmark.png){: style="width:64px; height:auto" }](https://github.com/pinnaculum/galacteek-installer/releases/download/ae658dfc/Galacteek-ae658dfc-x86_64.dmg) [Télécharger l'image DMG (MacOS)](https://github.com/pinnaculum/galacteek-installer/releases/download/ae658dfc/Galacteek-ae658dfc-x86_64.dmg){#release}

[![NSI](img/windows-logo.png){: style="width:64px; height:auto" }](https://github.com/pinnaculum/galacteek-installer/releases/download/ca9d79f0/Galacteek-ca9d79f0-installer-x86_64.exe) [Télécharger le NSI (Windows)](https://github.com/pinnaculum/galacteek-installer/releases/download/ca9d79f0/Galacteek-ca9d79f0-installer-x86_64.exe){#release}

[CHANGELOG](https://gitlab.com/galacteek/galacteek/-/blob/pimp-my-dweb/CHANGELOG.md) | [Telegram channel](https://t.me/Galacteek) | [Matrix room](https://matrix.to/#/#galacteek:matrix.org).

# Démarrage

Au premier lancement ou durant une mise à jour, vous devriez voir
une popup de ce style:

![type:video](./videos/galacteek-starter.gif)

# Flatpak bundle

Installez le bundle [Flatpak](https://flatpak.org/) sur Linux
comme utilisateur non-root en exécutant ces commandes dans un terminal:

```sh
--8<-- "flatpak-bundle-install.sh"
```

Ou bien, si vous ne voulez installer l'application que pour votre utilisateur:

```sh
--8<-- "flatpak-bundle-install-user.sh"
```

L'application apparaitra avec le nom *Galacteek* dans le sous-menu
*Internet* dans la liste des applications sur votre bureau.

Pour lancer *galacteek* depuis un terminal:

```sh
--8<-- "flatpak-run.sh"
```

# Lancement

## Installer gstreamer (Linux)

Pour utiliser le lecteur multimédia dans galacteek vous aurez besoin
d'installer la librarie *gstreamer* 1.0. Pour installer *gstreamer* 1.0
et les principaux plugins sur une machine Debian/Ubuntu, exécutez la
commande suivante:

```sh
sudo apt-get install gstreamer1.0-plugins-base gstreamer1.0-plugins-good gstreamer1.0-plugins-bad gstreamer1.0-plugins-ugly gstreamer1.0-libav gstreamer1.0-tools gstreamer1.0-x gstreamer1.0-alsa gstreamer1.0-gl gstreamer1.0-qt5 gstreamer1.0-pulseaudio
```

Pour tout autre distribution Linux, cherchez et installez les paquets *gstreamer* 1.0
et les plugins *good*, *bad* et *ugly*.

## Comment lancer l'AppImage (Linux)

Après avoir téléchargé le fichier *AppImage*, rendez-le exécutable et
lancez-le depuis le shell, ou double-cliquez simplement depuis le bureau.
Exemple (shell)::

```sh
--8<-- "appimage-run.sh"
```

## Comment lancer via Flatpak (Linux)

```sh
--8<-- "flatpak-run.sh"
```

## Comment lancer le DMG (MacOS)

Aprés avoir ouvert (*monté*) le fichier DMG, au clavier maintenez *Control*
et cliquez sur l'icone de *galacteek*, puis selectionnez **Ouvrir** et ensuite
acceptez. Vous devrez probablement autoriser le système MacOS a installer
des applications **from anywhere** dans les réglages de sécurité.
