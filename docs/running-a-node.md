---
title: Running with Docker
---

# How to run the docker image

To run a *galacteek* node, all you need is a Linux system with docker.

By running a node you can help with the testing of the **pronto** network.
To get an idea of how **pronto** works, [checkout this page](docs/pronto.md).

To pull the image and run a node in a Docker container, use the following
commands (*pimp-my-dweb* image):

```sh
mkdir $HOME/galacteek-docker
sudo docker pull registry.gitlab.com/galacteek/galacteek:pimp-my-dweb-latest-x11vnc
sudo docker run -p 5900:5900 -v $HOME/galacteek-docker:/home/galacteek -t registry.gitlab.com/galacteek/galacteek:pimp-my-dweb-latest-x11vnc
```

To see all the available docker images, checkout the
[container registry](https://gitlab.com/galacteek/galacteek/container_registry).

In this example, the data and graphs created by *galacteek* will be persisted
in the *$HOME/galacteek-docker* directory.

The password to access the *VNC* service is printed to the console when
starting the container. Find the IP address of your container with:

```sh
docker inspect <container-id>|grep IPAddress
```

You can now use any *VNC client* to access the node's UI (TCP port **5900**)

## Docker environment variables

### XVFB_WHD

Use the *XVFB_WHD* env var to set a different screen resolution for
the VNC service.  The default value is **1024x768x24**.

```sh
sudo docker run -p 5900:5900 -v $HOME/galacteek-docker:/home/galacteek -e XVFB_WHD="1280x1024x24" -t registry.gitlab.com/galacteek/galacteek:pimp-my-dweb-latest-x11vnc
```
