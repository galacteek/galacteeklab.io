[![logo](https://gitlab.com/galacteek/galacteek/-/raw/pimp-my-dweb/share/icons/galacteek.png){: style="width:256px; height:256px" }](https://gitlab.com/galacteek/galacteek)

[galacteek](https://gitlab.com/galacteek/galacteek) est un navigateur pour le
web distribué et une plateforme pour développer des *dapps* (applications
distribuées) P2P. *galacteek* repose sur la technologie
[IPFS](https://ipfs.io), qui est une *stack* P2P complète et un système
de fichiers décentralisé, partie intégrante du *Web 3.0*.

Les *dapps* P2P sont écrites avec le [language QML](https://en.wikipedia.org/wiki/QML)
et stockées sur des nodes IPFS et sont par conséquent facilement
répliquables. Pour plus d'information sur l'usage des *dapps*, veuillez
consulter [la documentation sur les dapps](docs/dapps/tutorial.md)

*galacteek* utilise des chaines d'objets LD (linked-data) basées sur RDF
(appelées *OntoloChains*) pour synchroniser un graphe global de contenu,
controlé par les utilisateurs du réseau.
[Télechargez une image snapshot](download.md#snapshots) pour l'essayer.

**Installation avec Flatpak**:

```sh
--8<-- "flatpak-bundle-install.sh"
```

[![Télécharger](download.png){: style="width:128px; height:128px" }](download.md)  [![Flatpak](img/flatpak-logo.jpg){: style="width:128px; height:auto" }](download.md#flatpak) [![Images Docker](docker.jpg){: style="width:128px; height:128px" }](running-a-node.md) [![Demos](demo.png){: style="width:128px; height:128px" }](demo.md) [![Canal Telegram](telegram.png){: style="width:128px; height:128px" }](https://t.me/Galacteek) [![Matrix room](matrix.svg){: style="width:128px; height:auto" }](https://matrix.to/#/#galacteek:matrix.org) [![Gitlab](img/gitlab.png){: style="width:128px; height:128px" }](development.md)
--8<-- "sponsor-platforms.md"

![type:video](./videos/galacteek-demo-main.mp4)

[Allez ici pour télécharger le logiciel](download.md){#download}

## Problèmes connus

- [#15](https://gitlab.com/galacteek/galacteek/-/issues/15): problème
  de persistence des dataset RDF lors du premier lancement de l'application.
  Relancer l'application règle le problème.

## Contact

Contact par mail [ici](mailto: galacteek@protonmail.com).

## Sponsoriser ce projet

Consultez [la page de sponsor](sponsor.md)
