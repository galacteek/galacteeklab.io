---
title: Sponsoriser ce projet
---

Vous pouvez aider ce projet en devenant un sponsor régulier
gràce à [Liberapay](https://liberapay.com/galacteek/donate) ou
[Ko-fi](https://ko-fi.com/galacteek) ou en faisant
une donation par cryptomonnaie. Merci!

--8<-- "sponsor-platforms.md"

# Bitcoin

BTC: **1Cgwbzi6R4TgWp7AG67BPwYY1iz15ATR7A**

![BTC donate](btc-donate.png)]

# Monero

XMR: **46CAwqF4zwjL3Pe1oxtr2AGbmzLFY4rp6eAGkiDbAfYnYCAx8C47dex2XGb6VqXDUoYEZmemLzKxT1wkjFU6ebk23DensvR**

# Ethereum

ETH: **0xaB3C8911D5C59bAdF797C546D7Fb56A00e4355dd**
