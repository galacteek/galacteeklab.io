---
title: Tutorial
---

# Dapps

In *galacteek*, dapps (also called *icapsules*) are written in QML, and can also
take advantage of traditional web interfaces thanks to the QML integration with
QtWebEngine (meaning you can write hybrid applications).

Capsules are deployed to IPFS and are referenced in the capsules registry.
The capsules and the registry are defined as YAML-LD, and they are then
turned into RDF.

## Hello world example

This is an *Hello world* app, but we display the *Content ID* of the text,
and not the text itself. Here we import the 
[gforce capsule](https://gitlab.com/galacteek/icapsule-qml-gforce).

This small example stores the content *Hello world* in
IPFS and shows the IPFS hash (*CID*) in a text field. The full program
lets you type in some text and see what the corresponding *CID* for
the text is.

```qml
import QtQuick 2.2
import GForce 1.0
import GForce.ipfs 1.0
import GForce.forms 1.0

GEmbeddedDapp {
  IpfsOperator {
    id: ipfsop
  }

  IText {
    id: helloCid
    color: 'red'
    font.pixelSize: 20
  }

  Connections {
    target: ipfsop

    function onOpDone(opid, result) {
      helloCid.text = result.Hash
    }
  }

  Component.onCompleted: {
    let rid = ipfsop.addStr('Hello world')
  }
}
```

The *IpfsOperator* object performs operations in IPFS in an *asynchronous*
manner, so that the QML UI is not blocked.

### Write the manifest

You'll need to write a manifest for the application.
The manifest should be named **manifest.yaml** and is
written as YAML-LD.

The manifest must have the following properties:

- **id**: This is the **uri** of the capsule manifest and it must
be unique. The URN *namespace* (the first part after *urn:*) can be
anything you want (doesn't need to be *glk*)
- **capsuleType**: the type of capsule. Possible values are:
*dapp-qml*, *lib-qml*
- **iconIpfsPath**: the IPFS CID/path of the dapp's icon
- **description**
- **latest**: **uri** of the latest release

The *releases* section of the manifest should list the capsule
releases. The **id** of your release must contain a valid
version at the end of the urn::

    urn:glk:dapps:examples:helloworld:1.0.1

The dependencies list can include version numbers or use
the *latest* query::

    depends:
      - urn:glk:libs:qml:somelib:1.0.0
      - urn:glk:libs:qml:gforce?=version=latest

A capsule contains one or more *components*. The definition
of each component should contain:

- **id**: This is the **uri** of the component
- **icapsule**: This is the **uri** of the capsule it's bound to
- **sourceType**: Where to load the capsule from: **local** or **ipfs**
- **fsPath**: If you're developing your dapp locally, put the path of
the component here (and use a *local* sourceType)
- **cid**: The CID of the tar-gzipped component 
- **qmlEntryPoint**: If your capsule is a dapp, this is the filename
of the QML code that will be executed when your dapp is run.

Here's the generated manifest.

```yaml
'@type': ICapsuleManifest

name: helloworld
id: urn:glk:dapps:examples:helloworld
uri: urn:glk:dapps:examples:helloworld
capsuleType: dapp-qml
iconIpfsPath: bafkreigkcazy7umh3dv7c2arwrbs5ni5t4qqfm6c5ybpbiekyjmzsiqsuq
description: Hello world dapp example

latest: urn:glk:dapps:examples:helloworld:1.0.4
stable: urn:glk:dapps:examples:helloworld:1.0.4

releases:
- '@type': ICapsule
  id: urn:glk:dapps:examples:helloworld:1.0.4
  manifest: urn:glk:dapps:examples:helloworld

  depends:
  - urn:glk:libs:qml:gforce?=version=latest

  usesComponent:
  - '@type': ICapsuleComponent
    id: urn:glk:dapps:examples:helloworld:1.0.4#core
    icapsule: urn:glk:dapps:examples:helloworld:1.0.4
    sourceType: ipfs
    cid: bafkreidkt36o655ru5nuiunubtnfntjlxka7nxukj2blxlxpnxnb2nuuxm
    qmlEntryPoint: HelloWorld.qml
```

You can checkout the
[git repository here](https://gitlab.com/galacteek/icapsule-dapp-helloworld/-/tree/purple)
for the complete dapp.

### Test your dapp

You will need to store the manifest in a local registry.
In your *home directory*, create a file called
*icapsule-registry-dev-purple.yaml* (*purple* is the default registry branch)
and store your manifest:

```yaml
'@context':
  '@vocab': 'ips://galacteek.ld/'

'@type': 'ICapsulesRegistry'

id: urn:glk:icapregs:local:myregistry
revision: 1

manifests:
  - '@type': ICapsuleManifest
    your manifest here ...
```

Restart *galacteek* and you should see all your capsules in the
*dapps* workspace.

## Web example

This example shows how to embed a webengine widget to interact with
the dweb. *DWebView* can access the same protocols as the browser
tabs in galacteek.

```qml
import QtQuick 2.2
import GForce 1.0
import GForce.ipfs 1.0

GEmbeddedDapp {
  id: dapp

  DWebView {
    id: dwebView

    url: 'ipfs://ipfs.io'
  }
}
```
