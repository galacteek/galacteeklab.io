# IPFS functions

## Operators

An IPFS operator is an object that can perform functions on the
IPFS node we're attached to.

```qml
import GForce.ipfs 1.0

IpfsOperator {
  id: ipfsop
}
```

With the **IpfsOperator**, some functions are synchronous
but most are asynchronous, and will return right away an operation
ID, performing the function in the background. You will be notified
when the operation is finished, with the opid and the result.

Store the operation id, and use **Connections** and the **opDone** signal
to get the results.

```qml
Connections {
  target: ipfsop

  function onOpDone(rid, result) {
    console.debug(rid)
  }
}
```

## Adding content

All these functions return an IPFS entry, with *Hash* and *Name*
inside the object, as returned by the IPFS daemon. It will
return an empty object if the operation fails.

### Adding a file

Use the **add(path, options)** asynchronous function. It returns
an IPFS entry, with *Hash* and *Name*. If you add a directory,
it will return the entry of the top directory.

```qml
let rid = ipfsop.add(path, {})
```

```qml
Connections {
  target: ipfsop

  function onOpDone(rid, result) {
    console.debug(result.Hash)
  }
}
```

The following options can be passed:

- *recursive*: boolean, defaults to *true*
- *only_hash*: Don't add the content, only compute CID. boolean, defaults to *false*
- *pin*: Pin the content. boolean, defaults to *true*
- *wrap*: Add a directory wrapper. boolean, defaults to *false*

```qml
let rid = ipfsop.add(path, { recursive: true, wrap: true })
```

### Adding a string value

Use the **addStr(strValue)** asynchronous function.

```qml
let rid = ipfsop.addStr('Hello')
```

You can also do it with the synchronous function and get the result
immediately. Realize that the synchronous versions will block
the Qt event loop, therefore only use them with small-sized data:

```qml
let result = ipfsop.addStrSync('Hello')
console.debug(result.Hash)
```

### Adding bytes

Use the **addBytes(bytes)** asynchronous function.

```qml
let rid = ipfsop.addBytes(bytesArray)
```

Synchronous version:

```qml
let result = ipfsop.addBytesSync(bytesArray)
console.debug(result.Hash)
```

### Getting the content of a UnixFS object

Use the **cat** asynchronous function. The result in the callback
is a JS ArrayBuffer object.

```qml
let rid = ipfsop.cat('/ipns/ipfs.io/index.html', {})
```

```qml
let rid = ipfsop.cat('/ipns/ipfs.io/index.html', {
  offset: 256,
  length: 16,
  timeout: 60
})
```

### Getting the MIME type of an object

```qml
let rid = ipfsop.detectMimeType(objPath, options)
```

```qml
let rid = ipfsop.detectMimeType('/ipns/ipfs.io/index.html', {timeout: 10})
```

### Pinning an object

```qml
let rid = ipfsop.pin(objPath, {timeout: 60})
```

### Pinning an object on a remote service

```qml
let rid = ipfsop.pinRemoteAdd(serviceName, objPath, {})
```

```qml
let rid = ipfsop.pinRemoteAdd(serviceName, objPath, {name: 'some-object'})
```

### Unpinning an object from a remote service

```qml
let rid = ipfsop.pinRemoteRemove(serviceName, {name: 'some-object'})
```

```qml
let rid = ipfsop.pinRemoteRemove(serviceName,
  {name: 'some-object', status: ['pinned']})
```

```qml
let rid = ipfsop.pinRemoteRemove(serviceName, {cid: 'bafy12oi...'})
```

### Get the list of remote pinning services

This call is synchronous and returns the list of remote pinning services
available on this node.

```qml
let services = ipfsop.pinRemoteServiceListSync()
```

## Pubsub

Creating a JSON pubsub channel is a simple task.

```qml
import GForce 1.0
import GForce.ipfs.pubsub 1.0

JsonPubsubChannel {
  topic: 'galacteek.dapps.helloworld'

  onChannelStart: {
    console.debug('Channel started on: ' + topic)

    send({
      'content': 'Hello world'
    })
  }

  onMessage: {
    // The JSON message is available from the 'jsonMsg' variable

    console.debug(JSON.stringify(jsonMsg))
  }
}
```

The **topic** string property should point to the IPFS pubsub topic name.

The **autoStart** boolean property defines if the pubsub channel should
listen right away on object creation, it defaults to *true*.

The **onMessage** signal is fired every time a message is received.
Use the **send()** function to send a JSON object on the topic.

### Curve25519 JSON channel

Using *X25519* encryption on a JSON channel is possible:

```qml
import GForce 1.0
import GForce.ipfs.pubsub 1.0

JsonPubsubChannel {
  topic: 'galacteek.dapps.curved'
  encryption: 'curve25519'
}
```
