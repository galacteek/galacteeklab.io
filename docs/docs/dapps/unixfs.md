# UnixFS

## UnixFS directory views

*UnixFsDirView* is a specific kind of *ListView* that lets you render
the contents of an IPFS UnixFS directory. In *dvoz*, the
*UnixFs sailor* tool (in the *Tools* menu) uses this.

Here's how to integrate it in your app:

```qml
import GForce 1.0
import GForce.views 1.0

Item {
  UnixFsDirView {
    anchors.fill: parent
    path: '/ipns/dist.ipfs.io'
  }
}
```

The *path* property controls the UnixFS folder that you want
to list. Use the *changeObject(path)* function to change the
object path.

### Stacking UnixFs views

The *UnixFsDirViewStacker* component automatically stacks
directory views (if you use *UnixFsDirView* alone, the model
is replaced when you browse a sub-directory).
