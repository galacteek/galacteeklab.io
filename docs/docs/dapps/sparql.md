---
title: SparQL API
---

# JSON-LD schemas

The main schemas catalog is **ips://galacteek.ld/** (accessible
with the **gs** SparQL prefix by default).

[Checkout the schemas here](https://gitlab.com/galacteek/galacteek-ld-web4/-/tree/master/galacteek_ld_web4/contexts/galacteek.ld)

# Using SparQL models in your dapp

## SparQL documentation

If you don't know SparQL yet here are a few resources to learn:

- [SparQL tutorial](https://www.stardog.com/tutorials/sparql/)
- [SparQL by example](https://www.w3.org/2009/Talks/0615-qbe/)

## Declaring your model

From your *dapp* it's very easy to run SparQL requests and use
the results as a Qt list model.

The [SparQLCoreModel](https://gitlab.com/galacteek/icapsule-qml-gforce/-/blob/purple/core/qml/GForce/models/sparql/SparQLCoreModel.qml) QML component
allows you to specify the SparQL query and graph on which to
run the query (if you don't specify *graphUri* it defaults to
*urn:ipg:i*, the top conjunctive graph).
The resulting model is compatible with Qt/QML's model/view system.

Here's an example model which retrieves a list of articles.
Note that we use the **gs** prefix, which maps to
**ips://galacteek.ld/**. You can consult all the available
schemas [here](https://gitlab.com/galacteek/galacteek-ld-web4/-/tree/master/galacteek_ld_web4/contexts/galacteek.ld).

```qml
import GForce.models.sparql 1.0

SparQLCoreModel {
  id: amodel
  graphUri: 'urn:ipg:i'

  query: "
    SELECT *
    WHERE {
        ?uri a gs:Article ;
          gs:headline ?headline ;
          gs:dateCreated ?dateCreated .
    }
    ORDER BY DESC(?dateCreated)
  "
}
```

## Bindings

You can use *bindings* when executing a request. *Bindings*
are variables that will be substituted when the query is run.
Here's an example where we filter articles with a regular expression.

```qml
import GForce.models.sparql 1.0

SparQLCoreModel {
  id: amodel
  query: "
    SELECT *
    WHERE {
        ?uri a gs:Article ;
          gs:headline ?headline ;
          gs:dateCreated ?dateCreated .
        FILTER(regex(?headline, str(?headlineRegexp), 'i'))
    }
  "

  bindings: { 'headlineRegexp': 'hello.*' }
}
```

Here we have a binding called *headlineRegexp*.

It's of course possible to change the bindings dynamically, but
do not forget to call the **run()** function after changing them.

```qml
amodel.bindings = { 'headlineRegexp': 'hello.*' }
amodel.run()
```

You will most likely always define your model in a separate QML
file (for example we could call it **ArticlesModel.qml**) and reuse it.

```qml
ArticlesModel {
  id: articles
  bindings: { 'headlineRegexp': 'hello.*' }
}
```

## Use the model in a view

It's then very easy to render your model in a view. The names of the Qt roles
are the same as the variables in your SparQL query, here we show
the *headline* in the delegate.

```qml
import QtQuick.Controls 1.2

ListView {
  id: view
  model: amodel

  delegate: Text {
    text: headline
  }
}
```

Please note that if one of your SparQL bindings is a **URI** (as with
the *uri* binding in the previous example), it will
be passed to the Qt model as a string and not as a special url type.

Have a look at [the gforce SparQL models](https://gitlab.com/galacteek/icapsule-qml-gforce/-/tree/purple/core/qml/GForce/models/sparql)
for more examples.
