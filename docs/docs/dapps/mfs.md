# Interacting with the MFS

The *GForce* capsule provides an API to interact with the IPFS
*Mutable File System*, using the *MfsView*. The *MfsView* is a customized
*TreeView*, which displays the content of the MFS. It provides
a signal that notifies you when the user selected a file in the MFS
view.

```qml
signal ipfsObjectSelected(string path, string url, string mimeType)
```

Usage:

```qml
import GForce.ipfs 1.0

Item {
  MfsView {
    anchors.fill: parent

    onIpfsObjectSelected: {
      // The user double-clicked on a file in the MFS view
      // path, url, mimeType
      console.debug(path)
    }
  }
}
```
